package com.arc.ishipDocs.Pages;

import java.awt.AWTException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.arc.ishipsDocs.utils.PropertyReader;
import com.arc.ishipsDocs.utils.SkySiteUtils;
import com.arc.ishipsDocs.utils.randomFileName;
import com.arcautoframe.utils.Log;


public class PlaceOrder_Page extends LoadableComponent<PlaceOrder_Page> {
	
	 WebDriver driver;
	static String orderNo = null;
	private boolean isPageLoaded;
	static int Avl_File_Count1 =0;
	
	/**
	 * Identifying web elements using FindBy annotatation 
	 */
	
	@FindBy(xpath="//i [@class='icon icon-print-order']")
	WebElement printordertab;
	

	@FindBy(xpath="//button[@id='btnSelectFiles_printOrder']")
	//@FindBy(xpath="(//button[contains(text(),'Select files')])[1]")
	WebElement SelectFiles;
	
	@FindBy(xpath="//button[@id='btnView1']")
	
	WebElement NextButton;
	
    @FindBy(xpath="//textarea[@id='txtPrintInstruction']")
	
	WebElement printinstruction;
	
	
	
	@FindBy(xpath="//*[@id='txtLocationAutoComplete']")
	WebElement txtAddress;
	
	@FindBy(xpath="//td[@data-name='ARCReproAddress']")
	WebElement txtAddress_link;
	
	@FindBy(xpath="//*[@id='txtName']")
	WebElement txtFullName;
	
	@FindBy(xpath="//*[@id='txtEmail']")	
	WebElement txtEmailAddress;
	
	
	@FindBy(xpath="//*[@id='txtCompanyName']")	
	WebElement CompanyName;
	
	@FindBy(xpath="//*[@id='txtPhone']")	
	WebElement PhoneNumber;
	
	@FindBy(xpath="//button[@id='btnView2Next']")	
	WebElement NextButton1;
	
	
	@FindBy(xpath="//*[@id='ddlDeliveryOption']")	
	WebElement DeliveryOption;
	
	@FindBy(xpath="//*[@id='ddlDeliveryOption']/option[3]")	
	WebElement DeliveryOption_3;
	
	
	
	
	@FindBy(xpath="//*[@id='txtAddress']")	
	WebElement DeliveryAddress;
	
	@FindBy(xpath="//*[@id='txtzip']")	
	WebElement Zip;
	
	@FindBy(xpath="(//i [@class='icon icon-calender-icon'])[1]")	
	WebElement duedate;
	
	@FindBy(xpath="//td [@class='next']/a[2]")	
	WebElement Next;
	
	@FindBy(xpath = "//a[@class='closenew']")
	WebElement calColseBtn;
	
	@FindBy(xpath="html/body/div[2]/table/tbody/tr[6]/td[5]")	
	WebElement Date_Click;
	
	@FindBy (xpath = "//iframe [@id='livechat-full-view']")
	WebElement chatiFrame;
	
	
	@FindBy (xpath = "//button[@class='btn']")
	WebElement CloseButton;	
	
	
	@FindBy(xpath="//a [@class='current']")	
	WebElement Today_date;
	
	@FindBy(xpath="//*[@id='timeSelect']")	
	WebElement TimeSelect;
	
	@FindBy(xpath="//option [@value='12:00 AM']")	
	WebElement Time;
	
	@FindBy(xpath="//option [@value='07:00 AM']")	
	WebElement Time1;
	
	
	@FindBy(xpath="//*[@id='wrapper']")	
	WebElement ChatPOPUp;
	
	@FindBy(xpath="//span[@class='icon-minimize']")	
	WebElement Chatminimize;
	
	@FindBy(xpath="//*[@id='txtPrintInstruction']")	
	WebElement PrintInstruction;
	
	
	@FindBy(xpath="//*[@id='btnSubmit']")	
	WebElement SubmitButton;
	

	@FindBy(xpath="//span [@id='spaOrderId']")	
	WebElement ORDERID;
	
	
	@FindBy(css="#txtUserId")	
	WebElement txtBoxUserName;
	
	@FindBy(css="#txtPassword")	
	WebElement txtBoxPassword;
	
	@FindBy(css="#btnLogin")	
	WebElement btnLogin;
	
	@FindBy(xpath="//*[@id='toolbar_navitem3']")
	WebElement OrderMonitor;
	
	@FindBy(xpath="//*[@id='mnuFilter']")
	WebElement SearchButton;	
	
	@FindBy(xpath="//*[@id='ddlField_1']")
	WebElement Field;
	
	@FindBy(xpath="(//option [@value='OD.OrderID^int'])[1]")
	WebElement Field_firstvalue;
	
	@FindBy(xpath=".//*[@id='ddlCondition_1']")
	WebElement Condition;	
	
	@FindBy(xpath="(//option [@value='Equals'])[1]")
	WebElement Condition_firstvalue;	
	
	@FindBy(xpath="//*[@id='txtValue_1']")
	WebElement value1;
	
	@FindBy(xpath="//*[@id='btnGo']")
	WebElement Go_button;	
	
	@FindBy(xpath="(//div[@id='divDownload'] )[1]")
	WebElement Download_link;
	
	
	@FindBy(xpath="//span[@ id='spanSelectedLocation']")
	WebElement Address_link;
	
	
	
	
	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, txtAddress, 60);
		
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}
	
	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * @param driver
	 * @return 
	 */
	public PlaceOrder_Page(WebDriver driver) 
	{
		   this.driver = driver;
		   PageFactory.initElements(this.driver, this);
	}
	
	
	
	/** 
	 * Method written for checking whether User Name text box is present?
	 * @return
	 */
	public boolean loginProjects()
	{
		SkySiteUtils.waitForElement(driver, txtAddress, 20);
		Log.message("Waiting for Username text box to be appeared");
		if(printordertab.isDisplayed())
			return true;
			else
			return false;
	}
	
	
	public String Printorder_Upload1() throws AWTException, InterruptedException, IOException, ClassNotFoundException, SQLException
	{
		//boolean result = false;
		String OrderId_InString = null;
		SkySiteUtils.waitTill(5000);
		//SkySiteUtils.waitForElement(driver, SelectFiles, 30);			
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_twenty");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);
		
		 JavascriptExecutor js = (JavascriptExecutor) driver;
		   js.executeScript("window.scrollBy(0,600)");
		   
		SkySiteUtils.waitTill(5000);
	    UploadFiles1(FolderPath, FileCount);
		Log.message("Select Files clicked Sucessfully");
		//======================================================================	
		List<WebElement> allElements = driver.findElements(By.xpath("//span[@class='filename']"));
		for (WebElement Element : allElements)
		{ 
			Avl_File_Count1 =Avl_File_Count1+1; 	
		}
		Log.message("Available File count of Print order ID is: "+Avl_File_Count1);		
		
		//=======================================================================
		SkySiteUtils.waitTill(5000);
		NextButton.click();
		
		String address = PropertyReader.getProperty("Address");
		if (Address_link.isDisplayed()){
				
		}
		else
		
		{
			SkySiteUtils.waitForElement(driver, txtAddress, 30);
			//String address = PropertyReader.getProperty("Address");
			txtAddress.clear();
			Log.message("Clear the Address Inbox Field");
			SkySiteUtils.waitForElement(driver, txtAddress, 30);
			SkySiteUtils.waitTill(5000);
			txtAddress.sendKeys(address);
			SkySiteUtils.waitForElement(driver, txtAddress_link, 30);
			SkySiteUtils.waitTill(1000);
			txtAddress_link.click();
			Log.message("Address: " + address + " " + "has been entered in Address text box.");
			SkySiteUtils.waitTill(8000);
			// For Stagging
			/*WebDriverWait wait = new WebDriverWait(driver, 10);
			if (driver.findElement(By.id("livechat-full-view")).isDisplayed()) {			
			driver.switchTo().frame("livechat-full-view");			
				SkySiteUtils.waitForElement(driver, Chatminimize,60);
				Chatminimize.click();	
				Log.message(" Chat Minimize option selected And Clicked ");			
			driver.switchTo().defaultContent();
			}*/
			SkySiteUtils.waitForElement(driver, txtFullName,60);
			txtFullName.clear();
			SkySiteUtils.waitTill(2000);
			String fullName = PropertyReader.getProperty("FullName");
			SkySiteUtils.waitForElement(driver, txtFullName,60);
			SkySiteUtils.waitTill(2000);
			txtFullName.sendKeys(fullName);
			Log.message("Full Name : " + fullName + " " + "has been entered in FullNmae text box.");
			SkySiteUtils.waitForElement(driver, txtEmailAddress, 20);
			txtEmailAddress.clear();
			String email = PropertyReader.getProperty("Email");
			txtEmailAddress.sendKeys(email);
			Log.message("Email : " + email + " " + "has been entered in email text box.");
			CompanyName.clear();
			String Company1 = PropertyReader.getProperty("Company");
			CompanyName.sendKeys(Company1);
			Log.message("Company : " + Company1 + " " + "has been entered in Company text box.");
			SkySiteUtils.waitForElement(driver, PhoneNumber, 20);
			PhoneNumber.clear();
			String phone= PropertyReader.getProperty("MobileNo");
			PhoneNumber.sendKeys(phone);
			Log.message("PHONE Number : " + phone + " " + "has been entered in Phone Number text box.");			
			NextButton1.click();
			Log.message("Nextbutton Clicked Sucessfully");
				
		}
		
		printinstruction.sendKeys("For testing purpose use only"); 
		SkySiteUtils.waitTill(5000);
		DeliveryOption.click();
		Log.message("How would you Like to deliver your order Inbox Clicked");
		DeliveryOption_3.click();
		Log.message("Delivery option selected And Clicked ");
		DeliveryAddress.clear();
		Log.message("Delivery Address Inbox cleared ");
		String Address= PropertyReader.getProperty("Delivery_Add");
		DeliveryAddress.sendKeys(Address);
		Log.message("Delivery Address : " + Address + " " + "has been entered in Delivery Address text box.");
		Zip.clear();
		String Zip1= PropertyReader.getProperty("ZIP");
		Zip.sendKeys(Zip1);
		Log.message("Entered ZIP Into Inbox field :-"+Zip1);	
		SkySiteUtils.waitForElement(driver, duedate,60);
		SkySiteUtils.waitTill(5000);
		JavascriptExecutor javascriptExecutor1 = (JavascriptExecutor) driver;
        javascriptExecutor1.executeScript("arguments[0].click();", duedate);
		
		Calendar cal = Calendar.getInstance();
		int currentDate = cal.get(Calendar.DATE);
		cal.add(Calendar.DATE, 2);
		int selectDate = cal.get(Calendar.DATE);
		//System.out.println(selectDate);
		String dateLoc = null ;
		if (currentDate >= 28){
			Next.click();
			SkySiteUtils.waitTill(5000);
			dateLoc = String.format("//a[text()='%d']",2);
		}else{
			Log.message("The selected Date is:---"+ selectDate);
			SkySiteUtils.waitTill(5000);
			dateLoc = String.format("//a[text()='%d']", selectDate);
			
		}
		Log.message("Sucessfully Date Selected:---");
		driver.findElement(By.xpath(dateLoc)).click();
		SkySiteUtils.waitTill(5000);		
	
		Log.message("Due Date Clicked Sucessfully ");
		
		SkySiteUtils.waitForElement(driver, TimeSelect,60);
		//Log.message("Particular Date Clicked Sucessfully ");
		//JavascriptExecutor javascriptExecutor2 = (JavascriptExecutor) driver;
       //javascriptExecutor2.executeScript("arguments[0].click();", TimeSelect);
        //Log.message("time Drop Down list  Clicked Sucessfully ");
		//TimeSelect.click();
		//SkySiteUtils.waitTill(5000);
		//Log.message("Time Drop Down Selected ");	
		///SkySiteUtils.waitForElement(driver, Time,60);
		SkySiteUtils.waitTill(5000);
		
		Select fruits = new Select(TimeSelect);
		//fruits.selectByVisibleText("2");
		fruits.selectByIndex(1);
		SkySiteUtils.waitTill(5000);
		//JavascriptExecutor javascriptExecutor3 = (JavascriptExecutor) driver;
        //javascriptExecutor3.executeScript("arguments[0].click();", Time);
		//Time.click();
		Log.message("Particular time Selected Sucessfully ");
		SkySiteUtils.waitForElement(driver, calColseBtn,60);
		calColseBtn.click();
		String PrintIns= PropertyReader.getProperty("printInstruction");
		SkySiteUtils.waitForElement(driver, PrintInstruction, 20);
		//PrintInstruction.clear();
		//PrintInstruction.sendKeys(PrintIns);
		//Log.message("Entered the Value In print Instruction Field ");
		SkySiteUtils.waitTill(2000);
		//SubmitButton.click();
		Log.message("Submit Button Clicked Sucessfully ");
		SkySiteUtils.waitTill(10000);
		System.out.println("completed !!!!!!!!!!!!!!!!");		
		
		SkySiteUtils.waitForElement(driver, ORDERID, 100);
		OrderId_InString = ORDERID.getText();
		int orderID = Integer.parseInt(ORDERID.getText());
		//int orderID
		Log.message("ORDERID:---"+orderID);
		//String PrintIns= PropertyReader.("printInstruction");
		
		//=================================================================
		

		String uName = PropertyReader.getProperty("UserID");
		String pWord = PropertyReader.getProperty("Password");
		String URL = PropertyReader.getProperty("CheckOrderURL");
		driver.get(URL);
		Log.message("URL Selected Sucessfully:----"+URL);

		
		
		SkySiteUtils.waitTill(5000);
		if(CloseButton.isDisplayed()){
		CloseButton.click();
	    }


		SkySiteUtils.waitTill(2000);		
		SkySiteUtils.waitForElement(driver, txtBoxUserName, 20);		
		txtBoxUserName.clear();
		SkySiteUtils.waitTill(2000);
		txtBoxUserName.sendKeys(uName);
		Log.message("Username: " + uName + " " + "has been entered in Username text box." );
		txtBoxPassword.clear();
		txtBoxPassword.sendKeys(pWord);
		Log.message("Password: " + pWord + " " + "has been entered in Password text box." );
		SkySiteUtils.waitForElement(driver, btnLogin, 20);
		btnLogin.click();
		Log.message("LogIn button clicked.");
		//SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitTill(2000);		
		SkySiteUtils.waitForElement(driver, OrderMonitor, 40);		
		OrderMonitor.click();		
		Log.message("Order Monitor Link Clicked Sucessfully" );
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, SearchButton, 40);	
		SearchButton.click();
		Log.message("Search Button Clicked Sucessfully" );
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Field, 40);
		Field.click();
		Log.message("Search Field Clicked Sucessfully" );
		SkySiteUtils.waitTill(5000);
		Field_firstvalue.click();
		Log.message("Search Field first Value Clicked Sucessfully" );
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, value1, 40);		
		value1.sendKeys(String.valueOf(orderID));
		Log.message("Order ID Entered In First Value field" );
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Go_button, 40);
		Go_button.click();
		Log.message("Go Button Clicked Sucessfully" );
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Download_link, 40);
		Download_link.click();
		SkySiteUtils.waitTill(5000);
		
		return OrderId_InString;
		
		
		   
	    }
	
	
	
	
	public String Printorder_Upload() throws AWTException, InterruptedException, IOException, ClassNotFoundException, SQLException
	{
		//boolean result = false;
		String OrderId_InString = null;
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, SelectFiles, 30);	
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    js.executeScript("window.scrollBy(0,600)");
		String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_MultipleFile");
		Log.message("FolderPath: " + FolderPath);
		String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
		int FileCount = Integer.parseInt(CountOfFilesInFolder);
		SkySiteUtils.waitTill(1000);
	    UploadFiles1(FolderPath, FileCount);
		Log.message("Select Files clicked Sucessfully");
		//======================================================================	
		List<WebElement> allElements = driver.findElements(By.xpath("//span[@class='filename']"));
		for (WebElement Element : allElements)
		{ 
			Avl_File_Count1 =Avl_File_Count1+1; 	
		}
		Log.message("Available File count of Print order ID is: "+Avl_File_Count1);		
		
		//=======================================================================
		SkySiteUtils.waitTill(5000);
		NextButton.click();
		
		String address = PropertyReader.getProperty("Address");
		if (Address_link.isDisplayed()){
				
		}
		else
		
		{
			SkySiteUtils.waitForElement(driver, txtAddress, 30);
			//String address = PropertyReader.getProperty("Address");
			txtAddress.clear();
			Log.message("Clear the Address Inbox Field");
			SkySiteUtils.waitForElement(driver, txtAddress, 30);
			SkySiteUtils.waitTill(5000);
			txtAddress.sendKeys(address);
			SkySiteUtils.waitForElement(driver, txtAddress_link, 30);
			SkySiteUtils.waitTill(1000);
			txtAddress_link.click();
			Log.message("Address: " + address + " " + "has been entered in Address text box.");
			SkySiteUtils.waitTill(8000);
			// For Stagging
			/*WebDriverWait wait = new WebDriverWait(driver, 10);
			if (driver.findElement(By.id("livechat-full-view")).isDisplayed()) {			
			driver.switchTo().frame("livechat-full-view");			
				SkySiteUtils.waitForElement(driver, Chatminimize,60);
				Chatminimize.click();	
				Log.message(" Chat Minimize option selected And Clicked ");			
			driver.switchTo().defaultContent();
			}*/
			SkySiteUtils.waitForElement(driver, txtFullName,60);
			txtFullName.clear();
			SkySiteUtils.waitTill(2000);
			String fullName = PropertyReader.getProperty("FullName");
			SkySiteUtils.waitForElement(driver, txtFullName,60);
			SkySiteUtils.waitTill(2000);
			txtFullName.sendKeys(fullName);
			Log.message("Full Name : " + fullName + " " + "has been entered in FullNmae text box.");
			SkySiteUtils.waitForElement(driver, txtEmailAddress, 20);
			txtEmailAddress.clear();
			String email = PropertyReader.getProperty("Email");
			txtEmailAddress.sendKeys(email);
			Log.message("Email : " + email + " " + "has been entered in email text box.");
			CompanyName.clear();
			String Company1 = PropertyReader.getProperty("Company");
			CompanyName.sendKeys(Company1);
			Log.message("Company : " + Company1 + " " + "has been entered in Company text box.");
			SkySiteUtils.waitForElement(driver, PhoneNumber, 20);
			PhoneNumber.clear();
			String phone= PropertyReader.getProperty("MobileNo");
			PhoneNumber.sendKeys(phone);
			Log.message("PHONE Number : " + phone + " " + "has been entered in Phone Number text box.");			
			NextButton1.click();
			Log.message("Nextbutton Clicked Sucessfully");
				
		}
		
		printinstruction.sendKeys("For testing purpose use only"); 
		SkySiteUtils.waitTill(5000);
		DeliveryOption.click();
		Log.message("How would you Like to deliver your order Inbox Clicked");
		DeliveryOption_3.click();
		Log.message("Delivery option selected And Clicked ");
		DeliveryAddress.clear();
		Log.message("Delivery Address Inbox cleared ");
		String Address= PropertyReader.getProperty("Delivery_Add");
		DeliveryAddress.sendKeys(Address);
		Log.message("Delivery Address : " + Address + " " + "has been entered in Delivery Address text box.");
		Zip.clear();
		String Zip1= PropertyReader.getProperty("ZIP");
		Zip.sendKeys(Zip1);
		Log.message("Entered ZIP Into Inbox field :-"+Zip1);	
		SkySiteUtils.waitForElement(driver, duedate,60);
		SkySiteUtils.waitTill(5000);
		duedate.click();
		Calendar cal = Calendar.getInstance();
		int currentDate = cal.get(Calendar.DATE);
		cal.add(Calendar.DATE, 2);
		int selectDate = cal.get(Calendar.DATE);
		//System.out.println(selectDate);
		String dateLoc = null ;
		if (currentDate >= 28){
			Next.click();
			SkySiteUtils.waitTill(5000);
			dateLoc = String.format("//a[text()='%d']",2);
		}else{
			Log.message("The selected Date is:---"+ selectDate);
			SkySiteUtils.waitTill(5000);
			dateLoc = String.format("//a[text()='%d']", selectDate);
			
		}
		Log.message("Sucessfully Date Selected:---");
		driver.findElement(By.xpath(dateLoc)).click();
		SkySiteUtils.waitTill(5000);		
	
		Log.message("Due Date Clicked Sucessfully ");
		/*SkySiteUtils.waitForElement(driver, Next,60);
		SkySiteUtils.waitTill(5000);
		Next.click();
		Log.message(" Date  Next Clicked Sucessfully ");
		SkySiteUtils.waitForElement(driver, Date_Click,60);
		Date_Click.click();*/
		SkySiteUtils.waitForElement(driver, TimeSelect,60);
		Log.message("Particular Date Clicked Sucessfully ");		
		TimeSelect.click();
		SkySiteUtils.waitTill(5000);
		Log.message("Time Drop Down Selected ");	
		SkySiteUtils.waitForElement(driver, Time,60);
		SkySiteUtils.waitTill(5000);
		Time.click();
		Log.message("Particular time Selected Sucessfully ");
		SkySiteUtils.waitForElement(driver, calColseBtn,60);
		calColseBtn.click();
		String PrintIns= PropertyReader.getProperty("printInstruction");
		SkySiteUtils.waitForElement(driver, PrintInstruction, 20);
		//PrintInstruction.clear();
		//PrintInstruction.sendKeys(PrintIns);
		//Log.message("Entered the Value In print Instruction Field ");
		SkySiteUtils.waitTill(2000);
		SubmitButton.click();
		Log.message("Submit Button Clicked Sucessfully ");
		SkySiteUtils.waitTill(10000);
		System.out.println("completed !!!!!!!!!!!!!!!!");		
		
		SkySiteUtils.waitForElement(driver, ORDERID, 100);
		OrderId_InString = ORDERID.getText();
		int orderID = Integer.parseInt(ORDERID.getText());
		//int orderID
		Log.message("ORDERID:---"+orderID);
		//String PrintIns= PropertyReader.("printInstruction");
		
		//=================================================================
		

		String uName = PropertyReader.getProperty("UserID");
		String pWord = PropertyReader.getProperty("Password");
		String URL = PropertyReader.getProperty("CheckOrderURL");
		driver.get(URL);
		Log.message("URL Selected Sucessfully:----"+URL);

		
		
		SkySiteUtils.waitTill(5000);
		if(CloseButton.isDisplayed()){
		CloseButton.click();
	    }


		SkySiteUtils.waitTill(2000);		
		SkySiteUtils.waitForElement(driver, txtBoxUserName, 20);		
		txtBoxUserName.clear();
		SkySiteUtils.waitTill(2000);
		txtBoxUserName.sendKeys(uName);
		Log.message("Username: " + uName + " " + "has been entered in Username text box." );
		txtBoxPassword.clear();
		txtBoxPassword.sendKeys(pWord);
		Log.message("Password: " + pWord + " " + "has been entered in Password text box." );
		SkySiteUtils.waitForElement(driver, btnLogin, 20);
		btnLogin.click();
		Log.message("LogIn button clicked.");
		//SkySiteUtils.waitTill(1000);
		SkySiteUtils.waitTill(2000);		
		SkySiteUtils.waitForElement(driver, OrderMonitor, 40);		
		OrderMonitor.click();		
		Log.message("Order Monitor Link Clicked Sucessfully" );
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, SearchButton, 40);	
		SearchButton.click();
		Log.message("Search Button Clicked Sucessfully" );
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Field, 40);
		Field.click();
		Log.message("Search Field Clicked Sucessfully" );
		SkySiteUtils.waitTill(5000);
		Field_firstvalue.click();
		Log.message("Search Field first Value Clicked Sucessfully" );
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, value1, 40);		
		value1.sendKeys(String.valueOf(orderID));
		Log.message("Order ID Entered In First Value field" );
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Go_button, 40);
		Go_button.click();
		Log.message("Go Button Clicked Sucessfully" );
		SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, Download_link, 40);
		Download_link.click();
		SkySiteUtils.waitTill(5000);
		
		return OrderId_InString;
		
		
		   
	    }
	
	
	
	
		public boolean validationfile() 
	{
		boolean result = false;
		SkySiteUtils.waitTill(5000);
		String parentHandle = driver.getWindowHandle();
		Log.message(parentHandle);
		for(String winHandle : driver.getWindowHandles())
		{
			driver.switchTo().window(winHandle);
		}
		SkySiteUtils.waitTill(5000);
		
		// ======================================================================
		int Avl_File_Count = 0;
		List<WebElement> AllElements= driver.findElements(By.xpath("//*[@id='grdRecipentDetails']/table/tbody/tr/td[2]"));
		for (WebElement element1 : AllElements)
		{
			SkySiteUtils.waitTill(5000);
			Avl_File_Count = Avl_File_Count + 1;
		}
		//Log.message("Available File count After Upload  is: " + Avl_File_Count);
		//driver.close();
		int After_avl_Count = Avl_File_Count-1;
		Log.message("After Available File count After Upload  is: " + After_avl_Count);
		
		//===========================================================
		/*String order = String.valueOf(orderID);
		DBConnectionAndValidation dbobj = new DBConnectionAndValidation();
		dbobj.DBconnect(order);	*/
		
		//===========================================================
		SkySiteUtils.waitTill(3000);		
		String status1 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[2]/td[5]")).getText();
		Log.message("status1 is :"+status1);
		String status2 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[3]/td[5]")).getText();
		Log.message("status2 is :"+status2);
		String status3 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[4]/td[5]")).getText();
		Log.message("status3 is :"+status3);
		String status4 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[5]/td[5]")).getText();
		Log.message("status4 is :"+status4);
		String status5 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[6]/td[5]")).getText();
		Log.message("status5 is :"+status5);
		String status6 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[7]/td[5]")).getText();
		Log.message("status6 is :"+status6);
		String status7 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[8]/td[5]")).getText();
		Log.message("status7 is :"+status7);
		String status8 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[9]/td[5]")).getText();
		Log.message("status8 is :"+status8);		
		String status9 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[10]/td[5]")).getText();
		Log.message("status9 is :"+status9);
		String status10 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[11]/td[5]")).getText();
		Log.message("status10 is:"+status10);
		String status11 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[12]/td[5]")).getText();
		Log.message("status11 is:"+status11);
		String status12 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[13]/td[5]")).getText();
		Log.message("status12 is:"+status12);
		String status13 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[14]/td[5]")).getText();
		Log.message("status13 is:"+status13);
		String status14 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[15]/td[5]")).getText();
		Log.message("status14 is:"+status14);
		String status15 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[16]/td[5]")).getText();
		Log.message("status15 is:"+status15);
		Log.message("Uploded File Count:---"+Avl_File_Count1);
		Log.message("Downloded File Count:----"+After_avl_Count);
		
		if((Avl_File_Count1== After_avl_Count)&&(status1.contains("Ready To Download"))&&(status2.contains("Ready To Download"))
				&&(status3.contains("Ready To Download"))&&(status4.contains("Ready To Download"))&&(status5.contains("Ready To Download"))
				&&(status6.contains("Ready To Download"))&&(status7.contains("Ready To Download"))&&(status8.contains("Ready To Download"))&&
				(status9.contains("Ready To Download"))&&(status10.contains("Ready To Download"))&&(status11.contains("Ready To Download"))
				&&(status12.contains("Ready To Download"))&&(status13.contains("Ready To Download"))&&(status14.contains("Ready To Download"))&&
				(status15.contains("Ready To Download")))
		{
			Log.message("Uploading File In Print order URL And After downloading File In ishipdocs Count is SAME" );
			//sendNotification() ;
			driver.close();
			driver.switchTo().window(parentHandle);
			SkySiteUtils.waitTill(10000);
			//driver.close();
			return true;
			
		}
		else
		{
			Log.message("Uploading File In Prinrorder URL And After downloading File In ishipdocs  Count is Not  same ");
			//sendNotification() ;
			
			return false;
			
		}
	    
	    
		
	}
		
		
		
		public boolean validationfile1() 
		{
			boolean result = false;
			SkySiteUtils.waitTill(5000);
			String parentHandle = driver.getWindowHandle();
			Log.message(parentHandle);
			for(String winHandle : driver.getWindowHandles())
			{
				driver.switchTo().window(winHandle);
			}
			SkySiteUtils.waitTill(5000);
			
			// ======================================================================
			int Avl_File_Count = 0;
			List<WebElement> AllElements= driver.findElements(By.xpath("//*[@id='grdRecipentDetails']/table/tbody/tr/td[2]"));
			for (WebElement element1 : AllElements)
			{
				SkySiteUtils.waitTill(5000);
				Avl_File_Count = Avl_File_Count + 1;
			}
			//Log.message("Available File count After Upload  is: " + Avl_File_Count);
			//driver.close();
			int After_avl_Count = Avl_File_Count-1;
			Log.message("After Available File count After Upload  is: " + After_avl_Count);
			
			//===========================================================
			/*String order = String.valueOf(orderID);
			DBConnectionAndValidation dbobj = new DBConnectionAndValidation();
			dbobj.DBconnect(order);	*/
			
			//===========================================================
			SkySiteUtils.waitTill(3000);		
			String status1 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[2]/td[5]")).getText();
			Log.message("status1 is :"+status1);
			String status2 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[3]/td[5]")).getText();
			Log.message("status2 is :"+status2);
			String status3 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[4]/td[5]")).getText();
			Log.message("status3 is :"+status3);
			String status4 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[5]/td[5]")).getText();
			Log.message("status4 is :"+status4);
			String status5 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[6]/td[5]")).getText();
			Log.message("status5 is :"+status5);
			String status6 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[7]/td[5]")).getText();
			Log.message("status6 is :"+status6);
			String status7 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[8]/td[5]")).getText();
			Log.message("status7 is :"+status7);
			String status8 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[9]/td[5]")).getText();
			Log.message("status8 is :"+status8);		
			String status9 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[10]/td[5]")).getText();
			Log.message("status9 is :"+status9);
			String status10 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[11]/td[5]")).getText();
			Log.message("status10 is:"+status10);
			String status11 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[12]/td[5]")).getText();
			Log.message("status11 is:"+status11);
			String status12 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[13]/td[5]")).getText();
			Log.message("status12 is:"+status12);
			String status13 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[14]/td[5]")).getText();
			Log.message("status13 is:"+status13);
			String status14 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[15]/td[5]")).getText();
			Log.message("status14 is:"+status14);
			String status15 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[16]/td[5]")).getText();
			Log.message("status15 is:"+status15);
			String status16 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[17]/td[5]")).getText();
			Log.message("status16 is:"+status16);
			String status17 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[18]/td[5]")).getText();
			Log.message("status17 is:"+status17);
			String status18 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[19]/td[5]")).getText();
			Log.message("status18 is:"+status18);
			String status19 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[20]/td[5]")).getText();
			Log.message("status19 is:"+status19);
			String status20 = driver.findElement(By.xpath(".//*[@id='grdRecipentDetails']/table/tbody/tr[21]/td[5]")).getText();
			Log.message("status20 is:"+status20);
			
			
			
			Log.message("Uploded File Count:---"+Avl_File_Count1);
			Log.message("Downloded File Count:----"+After_avl_Count);
			
			if((Avl_File_Count1== After_avl_Count)&&(status1.contains("Ready To Download"))&&(status2.contains("Ready To Download"))
					&&(status3.contains("Ready To Download"))&&(status4.contains("Ready To Download"))&&(status5.contains("Ready To Download"))
					&&(status6.contains("Ready To Download"))&&(status7.contains("Ready To Download"))&&(status8.contains("Ready To Download"))&&
					(status9.contains("Ready To Download"))&&(status10.contains("Ready To Download"))&&(status11.contains("Ready To Download"))
					&&(status12.contains("Ready To Download"))&&(status13.contains("Ready To Download"))&&(status14.contains("Ready To Download"))&&
					(status15.contains("Ready To Download")))
			{
				Log.message("Uploading File In Print order URL And After downloading File In ishipdocs Count is SAME" );
				//sendNotification() ;
				driver.close();
				driver.switchTo().window(parentHandle);
				SkySiteUtils.waitTill(10000);
				//driver.close();
				return true;
				
			}
			else
			{
				Log.message("Uploading File In Prinrorder URL And After downloading File In ishipdocs  Count is Not  same ");
				//sendNotification() ;
				
				return false;
				
			}
		    
		    
			
		}
		
		
		
		
	
	
	
	
	

	public void analyzeLog() 
	{
	    LogEntries logEntries = driver.manage().logs().get(LogType.BROWSER);
	    for (LogEntry entry : logEntries) 
	{
	                        System.out.println(new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage());
	            }
	}

	
	
	
	public void UploadFiles()throws InterruptedException, AWTException, IOException {

		SkySiteUtils.waitForElement(driver, SelectFiles, 60);
		SkySiteUtils.waitTill(2000);
		Log.message("Waiting for upload file button to be appeared");
		// SkySiteUtils.waitTill(5000);
		SelectFiles.click();
		SkySiteUtils.waitTill(3000);		
		// ====================Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath_new");
		File dest = new File(PropertyReader.getProperty("Upload_TestData_viewer_MultipleFile").toString());
		String path = dest.getAbsolutePath();
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + path);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(10000);
		//SkySiteUtils.waitForElement(driver, CreatePunch, 60);
		Log.message("Waiting for Cloud Account button to be appeared");
		SkySiteUtils.waitTill(5000);
		// ====================Delete the temp file=====================
	 
	}

	
	// File upload Method 
	
	public boolean UploadFiles1(String FolderPath, int FileCount) throws InterruptedException, AWTException, IOException
	{
		boolean result = false;
		SkySiteUtils.waitTill(3000);
		Log.message("Waiting for upload file button to be appeared");
		SkySiteUtils.waitForElement(driver, SelectFiles,30);
		//JavascriptExecutor javascriptExecutor1 = (JavascriptExecutor) driver;
        //javascriptExecutor1.executeScript("arguments[0].click();", SelectFiles);
		SelectFiles.click();
		Log.message("Select Files clicked Sucessfully");
		SkySiteUtils.waitTill(5000);
		//=================================	
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String Sys_Download_Path= "./File_Download_Location/"+ rn.nextFileName()+".txt" ;
			Log.message("system download location2"+Sys_Download_Path);
		String tmpFileName = Sys_Download_Path;
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		String expFilename = null;
		
		File[] files = new File(FolderPath).listFiles();
		for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(1000);
			}
		}

		output.flush();
		output.close();
		SkySiteUtils.waitTill(3000);
		// Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		File dest = new File(FolderPath);
		String path = dest.getAbsolutePath().toString();
		SkySiteUtils.waitTill(10000);
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + path + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(20000);
		
		try {
			File file = new File(tmpFileName);
			if (file.delete()) {
				Log.message(file.getName() + " is deleted!");
			} else {
				Log.message("Delete operation is failed.");
			}
		} catch (Exception e) {
			Log.message("Exception occured!!!" + e);
		}
		
		
		
						
		return result;
		
		
		
	}
	
	
	
	
	public static  void sendNotification() {

		 String from = PropertyReader.getProperty("FROM_Email");
	     String pass = PropertyReader.getProperty("FROM_Password");
	     String[] to = PropertyReader.getProperty("TO_Mail").split(",");
	     String[] ary = "abc".split("");
	     String subject = PropertyReader.getProperty("SUBJECT");
	     String body = PropertyReader.getProperty("BODY");

	     sendFromGMail(from, pass, to, subject, body);
	 }

	 private static void sendFromGMail(String from, String pass, String[] to, String subject, String body) {
	     Properties props = System.getProperties();
	     String host = "smtp.gmail.com";
	     props.put("mail.smtp.starttls.enable", "true");
	     props.put("mail.smtp.host", host);
	     props.put("mail.smtp.user", from);
	     props.put("mail.smtp.password", pass);
	     props.put("mail.smtp.port", "587");
	     props.put("mail.smtp.auth", "true");

	     Session session = Session.getDefaultInstance(props);
	     MimeMessage message = new MimeMessage(session);

	     try {
	         message.setFrom(new InternetAddress(from));
	         InternetAddress[] toAddress = new InternetAddress[to.length];

	         // To get the array of addresses
	         for( int i = 0; i < to.length; i++ ) {
	             toAddress[i] = new InternetAddress(to[i]);
	         }

	         for( int i = 0; i < toAddress.length; i++) {
	             message.addRecipient(Message.RecipientType.TO, toAddress[i]);
	         }

	         message.setSubject(subject);
	         message.setText(body);
	         Transport transport = session.getTransport("smtp");
	         transport.connect(host, from, pass);
	         transport.sendMessage(message, message.getAllRecipients());
	         transport.close();
	     }
	     catch (AddressException ae) {
	         ae.printStackTrace();
	     }
	     catch (MessagingException me) {
	         me.printStackTrace();
	     }
	 }

	

	
	public PlaceOrder_Page  ValidateFileCount() throws InterruptedException, AWTException, IOException
	{
		SkySiteUtils.waitTill(5000);		
		int Avl_File_Count=0;
		List<WebElement> allElements = driver.findElements(By.xpath(".//*[@id='ulSelector']/li/div/div[1]/div/div[1]/h6/span"));
		for (WebElement Element : allElements)
		{ 
			Avl_File_Count =Avl_File_Count+1; 	
		}
		Log.message("Available File count is: "+Avl_File_Count);
		SkySiteUtils.waitTill(5000);
		
		
		return new PlaceOrder_Page(driver).get();
	}
	
	
	
}
