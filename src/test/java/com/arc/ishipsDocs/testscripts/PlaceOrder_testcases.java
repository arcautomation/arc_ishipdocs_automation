package com.arc.ishipsDocs.testscripts;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.ishipDocs.Pages.PlaceOrder_Page;
import com.arc.ishipsDocs.utils.CommonMethod;
import com.arc.ishipsDocs.utils.PropertyReader;
import com.arcautoframe.utils.Log;



public class PlaceOrder_testcases {
	public static WebDriver driver;
	PlaceOrder_Page placeOrder_Page;
	
	@Parameters("browser")

	@BeforeMethod
	public WebDriver beforeTest(String browser) {

		if (browser.equalsIgnoreCase("firefox")) {
			File dest = new File("./drivers/win/geckodriver.exe");
			// System.setProperty("webdriver.gecko.driver",
			// dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("PlaceOrderURL"));
		}

		else if (browser.equalsIgnoreCase("chrome")) {
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory", "C:" + File.separator + "Users" + File.separator
					+ System.getProperty("user.name") + File.separator + "Downloads");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			options.setExperimentalOption("prefs", prefs);
			driver = new ChromeDriver(options);
			String URL= PropertyReader.getProperty("PlaceOrderURL");
			driver.get(PropertyReader.getProperty("PlaceOrderURL"));
			//Log.message(URL);

		}

		else if (browser.equalsIgnoreCase("safari")) {
			System.setProperty("webdriver.safari.noinstall", "false");
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("PlaceOrderURL"));
		}
		return driver;
	}
	
	
	 /* TC_001:-----Requiring upload And validation By: Scripted Ranjan */

		@Test(priority = 0, enabled = true, description = "Recurring upload And validation")
		public void FileUploadAndValidate() throws Throwable 
		{
			try 
			{ 				//for(int i=0;i<=1000;i++){
				
				Log.testCaseInfo("TC_001:-Recurring upload And validation");
				placeOrder_Page = new PlaceOrder_Page(driver).get();			
				placeOrder_Page.Printorder_Upload();
				//Log.assertThat(placeOrder_Page.Printorder_Upload(), "File uploaded sucessfully","File Not uploaded  Successfully ", driver);
				System.out.println("Script Ends Here !!!!!!!!");				
				Log.assertThat(placeOrder_Page.validationfile(), "File Count  Verified Successfully","File Count Not Verified Successfully ", driver);
			}
			//}
			catch (Exception e) 
			{
				CommonMethod.analyzeLog(driver);
				//placeOrder_Page = new PlaceOrder_Page(driver).get();
				//PlaceOrder_Page.sendNotification();				
				e.getCause();
				Log.exception(e, driver);
				
				//driver.close();
			} finally
			{
				Log.endTestCase();
				driver.quit();
			}

			
		}}

